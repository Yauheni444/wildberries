import os

from django.core.files.storage import FileSystemStorage
from wildberries.settings import BASE_DIR


class ObjFile(object):

    def __init__(self, file):
        self.FILE = file
        self.URL_FILE = ''

    def save_file(self):
        fs = FileSystemStorage()
        filename = fs.save(self.FILE.name, self.FILE)
        self.URL_FILE = fs.url(filename)

    def delete_file(self):
        pass

    def get_url_file(self, full=False):
        if full:
            return '%s%s' % (BASE_DIR, self.URL_FILE)
        else:
            return self.URL_FILE
