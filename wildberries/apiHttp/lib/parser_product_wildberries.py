import asyncio
import aiohttp
from pprintpp import pprint as PR


class ParsProductWildberries(object):
    URL_SITE = 'https://wbx-content-v2.wbstatic.net/ru/'

    def __init__(self, list_article):
        self.LIST_ARTICLE = list_article
        self.LIST_PRODUCT = []

    async def fetch_content(self, url, session):
        async with session.get(url) as response:
            data_json = await response.json()
            data = {
                'Article': data_json.get('nm_id'),
                'Brand': data_json.get('selling').get('brand_name'),
                'Title': data_json.get('imt_name')
            }
            self.LIST_PRODUCT.append(data)

    async def main(self):
        tasks = []
        async with aiohttp.ClientSession() as session:
            for i in range(len(self.LIST_ARTICLE)):
                item = self.LIST_ARTICLE[i-1]
                url = f'{self.URL_SITE}{item}.json'
                task = asyncio.create_task(self.fetch_content(url, session))
                tasks.append(task)
            await asyncio.gather(*tasks)

    def run(self):
        asyncio.run(self.main())
        return self.LIST_PRODUCT

