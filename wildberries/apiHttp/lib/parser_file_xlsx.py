import openpyxl
import os.path


class ParserFileXLSX(object):

    def __init__(self, path_file):
        self.FILE_PATH = path_file

    def get_data_file(self):
        if os.path.exists(self.FILE_PATH):
            file = openpyxl.open(self.FILE_PATH, read_only=True)
            sheet = file.active
            list_data = []
            column = 0
            for row in range(1, sheet.max_row):
                data = sheet[row][column].value
                list_data.append(str(data))
            return list_data
