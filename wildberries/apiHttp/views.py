import os
from pprintpp import pprint as PR
from django.views.generic.base import TemplateView
from rest_framework.response import Response
from rest_framework.views import APIView
from apiHttp.forms import ParserForm
from apiHttp.lib.file import ObjFile
from apiHttp.lib.parser_file_xlsx import ParserFileXLSX
from apiHttp.lib.parser_product_wildberries import ParsProductWildberries


class HomePageView(TemplateView):
    template_name = "apiHttp/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['h1'] = 'Парсер wildberries.ru'
        context['form'] = ParserForm()
        return context


class ApiDataProduct(APIView):

    def post(self, request):
        data_form = request.data

        file = data_form.get('file')
        article = data_form.get('article')

        list_article = []

        if not file and not article:
            return Response({
                'status': 'error',
                'message': 'no article',
                'data': []
            })

        if file:
            my_file = ObjFile(file)
            # сохранили файл
            my_file.save_file()
            # путь до файла
            path_file = my_file.get_url_file(True)
            file_xlsx = ParserFileXLSX(path_file)
            # список артикулов
            list_article = file_xlsx.get_data_file()
            # после парсинга удаляем
            os.remove(path_file)
        else:
            # список артикулов
            list_article.append(article)

        if len(list_article) <= 0:
            return Response({
                'status': 'success',
                'message': 'Transferred 0 articles',
                'data': []
            })

        # запросы на сайт
        pars_product = ParsProductWildberries(list_article)
        data_list_product = pars_product.run()

        return Response({
            'status': 'success',
            'message': '',
            'count': len(data_list_product),
            'data': data_list_product
        })