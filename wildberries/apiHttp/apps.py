from django.apps import AppConfig


class ApihttpConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apiHttp'
