from django import forms


class ParserForm(forms.Form):
    file = forms.FileField(required=False, label='Файл', widget=forms.FileInput(attrs={'class': 'form-control-file'}))
    article = forms.CharField(max_length=10, required=False, label='Артикул', widget=forms.TextInput(attrs={'class': 'form-control'}))
