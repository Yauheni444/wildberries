from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path
from apiHttp.views import HomePageView, ApiDataProduct
from django.conf import settings


urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path(r'api/v1/get_data_product/', ApiDataProduct.as_view(), name='product')
]


if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns() + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)