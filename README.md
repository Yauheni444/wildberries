# wildberries

## Running Locally

```bash
git clone https://gitlab.com/Yauheni444/wildberries.git
```

```bash
pip install -r requirements.txt
```

```bash
python manage.py migrate
```

```bash
python manage.py runserver
```
